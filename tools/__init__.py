import subprocess


def ping(host, logger, timeout=10):
    """Ugly hack to get ping time of host."""
    assert(isinstance(timeout, int))

    cmd = ['ping', '-c', '1', '-W', str(timeout), host]
    process = subprocess.Popen(
        cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, _ = process.communicate()
    exitcode = process.returncode

    if exitcode:
        logger.error('{}|failed'.format(host))
        return

    lines = out.decode('utf-8').splitlines()

    # Extract first (and only) ping result and extract ping time
    ping_line = lines[1]
    ping_time = ' '.join(ping_line.split()[-2:])

    logger.info('{}|{}'.format(host, ping_time))


def iperf(host, logger):
    """Checks speed of Internet connection."""

    cmd = ['iperf', '-c', host]
    process = subprocess.Popen(
        cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, _ = process.communicate()
    exitcode = process.returncode

    if exitcode:
        logger.error('{}|failed'.format(host))
        return

    lines = out.decode('utf-8').splitlines()

    # Extract first (and only) ping result and extract ping time
    speed_line = lines[-1]
    speed = ' '.join(speed_line.split()[-2:])
    logger.info('{}|{}'.format(host, speed))
