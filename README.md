# LOG NETWORK

## What is it?
Simple tool to check network connection continuous.

## Prerequisites

- Python 3
- iperf
- Python modules listed in requirements.txt

## Installation

No need to install. Make sure you have all the OS level
prerequisites. Install the Python modules listed in requirements.txt
by executing (make sure you are inside log_network directory):

`pip install -r requirements.txt`

You probably want to use a virtual environment.

## How to use it
Just run the command `./log_network.py`.

Check to logfile that are created.

## Author
Niclas Nilsson