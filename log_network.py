#!/usr/bin/env python3

from datetime import timedelta

from timeloop import Timeloop

from loggers import setup_loggers
from tools import ping, iperf


tl = Timeloop()

LOG_FILENAME = 'network.log'
LOG_FORMAT = '%(name)s|%(asctime)s|%(levelname)s|%(message)s'

loggers = setup_loggers(LOG_FORMAT, LOG_FILENAME)


##############
# Setup jobs #
##############


# Pings Google DNS server once every n seconds and logs it.
@tl.job(interval=timedelta(seconds=15))
def setup_ping_google():
    server = '8.8.8.8'
    ping(server, loggers['ping'])


# Pings Cloudflare DNS server once every n seconds and logs it.
@tl.job(interval=timedelta(seconds=15))
def setup_ping_cloudflare():
    server = '1.1.1.1'
    ping(server, loggers['ping'])


# Runs a iperf speed test against some french server once every n seconds.
@tl.job(interval=timedelta(seconds=900))
def setup_iperf_pingonline():
    iperf('ping.online.net', loggers['iperf'])


##################
# Start the loop #
##################

if __name__ == "__main__":
    tl.start(block=True)
