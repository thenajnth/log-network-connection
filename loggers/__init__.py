import logging


def setup_loggers(log_format, log_filename):
    logging.basicConfig(format=log_format, filename=log_filename,
                        level=logging.INFO)

    return {
        'ping': logging.getLogger('ping'),
        'iperf': logging.getLogger('iperf')
    }
